<?php
	class WordFrequencyCounter
	{
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $text = '';
		private $stopWordsArr = array();
		private $words_to_count_arr = array();
		private $contextsMax = 0;
		private $result_table = '';
		private $result_text_by_amount = '';
		private $result_text_by_alphabet = '';
		private $result_text_reverse_dictionary = '';
		private $caseSensitive = '';
		private $contextSensitive = '';
		private $unique_words_array = array();
		private $reverse_unique_words_array = array();
		private $count_words_in_text = 0;
		private $count_unique_words_in_text = 0;
		private $words = array();
		const BR = "<br />\n";
		
		function __construct($text, $stopWords, $words_to_count, $mainCharacters = '', $additionalCharacters = '', $contextsMax = 0, $caseSensitive = 0, $contextSensitive = 0)
		{
			$this->text = stripslashes($text);
			$this->caseSensitive = $caseSensitive;
			$this->contextSensitive = $contextSensitive;
			
			$stopWordsArr = preg_split("/\s+/", $stopWords);
			foreach($stopWordsArr as $stopWord)
			{
				if($this->caseSensitive)
				{
					$this->stopWordsArr[$stopWord] = 1;
				}
				else
				{
					$stopWord = mb_strtolower($stopWord, 'UTF-8');
					$this->stopWordsArr[$stopWord] = 1;
				}
			}
			
			if(!empty($words_to_count))
			{
				$words_to_count_arr = preg_split("/\s+/", $words_to_count);
				foreach($words_to_count_arr as $word_to_count)
				{
					$this->words_to_count_arr[$word_to_count] = 1;
				}
			}
			
			if(!empty($contextsMax) && preg_match("/^[1-9]\d*$/", $contextsMax))
			{
				$this->contextsMax = $contextsMax;
			}
			else $this->contextsMax = 0;
			
			$this->mainCharacters = preg_quote($mainCharacters, "-");
			$this->additionalCharacters = preg_quote($additionalCharacters, "-");
			
			$this->count_words_in_text = 0;
			$this->count_unique_words_in_text = 0;
		}
		
		public static function loadLanguages()
		{
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files))
			{
				foreach($files as $file)
				{
					if(substr($file, 2, 4) == '.txt')
					{
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang)
		{
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false)
			{
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line)
			{
				if(empty($line))
				{
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//')
				{
					if(empty($key))
					{
						$key = $line;
					}
					else
					{
						if(!isset(self::$localizationArr[$key]))
						{
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg)
		{
			if(isset(self::$localizationArr[$msg]))
			{
				return self::$localizationArr[$msg];
			}
			else
			{
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang)
		{
			if(!empty(self::$localizationErr))
			{
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Word Frequency Counter';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/WordFrequencyCounter/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		/* returns array{	[0]=>{	[0]=>word_and_separator,	[1]=>word,	[2]=>separator	}
							[1]=>{	... */
		public function getWords($string, $character_group_1, $character_group_2 = '')
		{
			$words_and_separators = array();
			if(!empty($character_group_1))
			{
				$pattern = "/(^|[$character_group_1][$character_group_1$character_group_2]*)([^$character_group_1]*)/u";
				preg_match_all($pattern, $string, $words_and_separators, PREG_SET_ORDER);
			}			
			return $words_and_separators;
		}
		
		public function run()
		{
			$paragraphsArr = preg_split("/\n/u", $this->text);
			foreach($paragraphsArr as $paragraph)
			{
				if(empty($paragraph)) continue;
				$wordsArr = $this->getWords($paragraph, $this->mainCharacters, $this->additionalCharacters);
				$wordsCnt = count($wordsArr);
				if(!empty($wordsArr))
				{
					$this->count_words_in_text += $wordsCnt - 1;
				}
				
				for($i = 0; $i < $wordsCnt; $i++)
				{
					if(!empty($wordsArr[$i][1]))
					{
						if($this->caseSensitive == 0)
						{
							$word = mb_strtolower($wordsArr[$i][1], 'UTF-8');
						}
						else
						{
							$word = $wordsArr[$i][1];
						}
						if(empty($this->stopWordsArr[$word]))
						{
							if(empty($this->words_to_count_arr) || (!empty($this->words_to_count_arr) && isset($this->words_to_count_arr[$word])))
							{
								if(!isset($this->unique_words_array[$word]['cnt']))
								{
									$this->unique_words_array[$word]['cnt'] = 1;
								}
								else
								{
									$this->unique_words_array[$word]['cnt']++;
								}
								if($this->unique_words_array[$word]['cnt'] <= $this->contextsMax)
								{
									$leftContext = '';
									$leftContext .= isset($wordsArr[$i-4]) ? '… ' : '';
									$leftContext .= isset($wordsArr[$i-3]) ? $wordsArr[$i-3][1] . $wordsArr[$i-3][2] : '';
									$leftContext .= isset($wordsArr[$i-2]) ? $wordsArr[$i-2][1] . $wordsArr[$i-2][2] : '';
									$leftContext .= isset($wordsArr[$i-1]) ? $wordsArr[$i-1][1] . $wordsArr[$i-1][2] : '';
									$rightContext = '';
									$rightContext .= isset($wordsArr[$i][2]) ? $wordsArr[$i][2] : '';
									$rightContext .= isset($wordsArr[$i+1]) ? $wordsArr[$i+1][1] . $wordsArr[$i+1][2] : '';
									$rightContext .= isset($wordsArr[$i+2]) ? $wordsArr[$i+2][1] . $wordsArr[$i+2][2] : '';
									$rightContext .= isset($wordsArr[$i+3]) ? $wordsArr[$i+3][1] . $wordsArr[$i+3][2] : '';
									$rightContext .= isset($wordsArr[$i+4]) ? ' …' : '';
									
									$this->unique_words_array[$word]['leftContext'][] = $leftContext;
									$this->unique_words_array[$word]['rightContext'][] = $rightContext;
									$this->unique_words_array[$word]['context'][] = $leftContext . '<font color="red">' . $wordsArr[$i][1] . '</font>' . $rightContext;
								}
							}
						}
					}
				}
			}
			
			$this->count_unique_words_in_text = count($this->unique_words_array);
			
			$this->result_table = '';
			if(!empty($this->unique_words_array))
			{
				if($this->contextSensitive)
				{
					$this->result_table .= '<table id="resultTableId" class="table table-sm table-striped sortable">';
					if($this->contextsMax > 0)
					{
						$this->result_table .= '<thead><tr>';
						$this->result_table .= '<th scope="col" class="sorttable_numeric" style="text-align: center">' . self::showMessage('frequency') . '</th>';
						$this->result_table .= '<th scope="col" class="sorttable_alpha" style="text-align: right">' . self::showMessage('left contexts') . ' ' . $this->contextsMax . ')</th>';
						$this->result_table .= '<th scope="col" class="sorttable_alpha" style="text-align: center">' . self::showMessage('word') . '</th>';
						$this->result_table .= '<th scope="col" class="sorttable_alpha" style="text-align: left">' . self::showMessage('right contexts') . ' ' . $this->contextsMax . ')</th>';
						$this->result_table .= '</tr></thead><tbody>';
					}
					else
					{
						$this->result_table .= '<thead><tr>';
						$this->result_table .= '<th scope="col" class="sorttable_numeric" style="text-align: center">' . self::showMessage('frequency') . '</th>';
						$this->result_table .= '<th scope="col" class="sorttable_alpha" style="text-align: center">' . self::showMessage('word') . '</th>';
						$this->result_table .= '</tr></thead><tbody>';
					}
					foreach($this->unique_words_array as $word => $wordInfoArr)
					{
						$leftContexts = '';
						$rightContext = '';
						$this->result_table .=	'<tr valign="top">';
						$this->result_table .=	'<td width=100 align="center"><b>' . $wordInfoArr['cnt'] . '</b></td>';
						if($this->contextsMax > 0)
						{
							foreach($wordInfoArr['leftContext'] as $context)
							{
								$leftContexts .= $context . "<br />";
							}
							$this->result_table .=	'<td align="right">' . $leftContexts . '</td>';
						}
						$this->result_table .=	'<td width=150 align="center"><font color="red">' . $word . '</font></td>';
						if($this->contextsMax > 0)
						{
							foreach($wordInfoArr['rightContext'] as $context)
							{
								$rightContext .= $context . "<br />";
							}
							$this->result_table .=	'<td>' . $rightContext . '</td>';
						}
						$this->result_table .=	'</tr>';
					}
					$this->result_table .=  '</tbody></table>';
				}
				else
				{
					$this->result_table .= '<table class="sortable" id="resultTableId">';
					if($this->contextsMax > 0)
					{
						$this->result_table .= '<thead><tr>';
						$this->result_table .= '<th scope="col" class="sorttable_numeric">' . self::showMessage('frequency') . '</th>';
						$this->result_table .= '<th scope="col" class="sorttable_alpha">' . self::showMessage('word') . '</th>';
						$this->result_table .= '<th scope="col" class="sorttable_alpha">' . self::showMessage('contexts') . ' ' . $this->contextsMax . ')</th>';
						$this->result_table .= '</tr></thead><tbody>';
					}
					else
					{
						$this->result_table .= '<thead><tr>';
						$this->result_table .= '<th scope="col" class="sorttable_numeric">' . self::showMessage('frequency') . '</th>';
						$this->result_table .= '<th scope="col" class="sorttable_alpha">' . self::showMessage('word') . '</th>';
						$this->result_table .= '</tr></thead><tbody>';
					}
					foreach($this->unique_words_array as $word => $wordInfoArr)
					{
						$сontexts = '';
						$this->result_table .=	'<tr valign="top">';
						$this->result_table .=	'<td align="center"><b>' . $wordInfoArr['cnt'] . '</b></td>';
						$this->result_table .=	'<td align="center"><font color="red">' . $word . '</font></td>';
						if($this->contextsMax > 0)
						{
							foreach($wordInfoArr['context'] as $context)
							{
								$сontexts .= $context . "<br />";
							}
							$this->result_table .=	'<td>' . $сontexts . '</td>';
						}
						$this->result_table .=	'</tr>';
					}
					$this->result_table .=  '</tbody></table>';
				}
			}
		}

		public function saveCacheFiles()
		{
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$dateCode = date('Y-m-d_H-i-s', time());
			$randCode = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			$serviceName = 'WordFrequencyCounter';
			$sendersName = 'Word Frequency Counter';
			$recipient = "corpus.by@gmail.com";
			$subject = "$sendersName from IP $ip";
			$mailBody = "Вітаю, гэта corpus.by!" . self::BR;
			$mailBody .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			$textLength = mb_strlen($this->text);
			$pages = round($textLength/2300, 1);
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_in.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", '', $this->text);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=in&f=$filename";
			if(mb_strlen($cacheText))
			{
				$mailBody .= "Тэкст ($textLength сімв., прыкладна $pages ст. па 2300 сімв. на старонку) пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$str = str_replace("\n", self::BR, trim($matches[0]));
				if(mb_strlen($str) < 300)
					$mailBody .= '<blockquote><i>' . $str . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				else
					$mailBody .= '<blockquote><i>' . mb_substr($str, 0, 300) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
			}
			$mailBody .= self::BR;

			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_in_cg1.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", '', $this->mainCharacters);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=in&f=$filename";
			if(mb_strlen($cacheText))
			{
				$mailBody .= "Спіс сімвалаў, з якіх можа складацца слова: " . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$str = str_replace("\n", self::BR, trim($matches[0]));
				if(mb_strlen($str) < 300)
					$mailBody .= '<blockquote><i>' . $str . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				else
					$mailBody .= '<blockquote><i>' . mb_substr($str, 0, 300) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
			}
			$mailBody .= self::BR;

			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_in_cg2.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", '', $this->additionalCharacters);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=in&f=$filename";
			if(mb_strlen($cacheText))
			{
				$mailBody .= "Спіс сімвалаў, з якіх можа складацца, але не можа пачынацца слова: " . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$str = str_replace("\n", self::BR, trim($matches[0]));
				if(mb_strlen($str) < 300)
					$mailBody .= '<blockquote><i>' . $str . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				else
					$mailBody .= '<blockquote><i>' . mb_substr($str, 0, 300) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
			}
			$mailBody .= self::BR;
						
			$mailBody .= 'Усяго знойдзена слоў у тэксце: ' . $this->count_words_in_text . self::BR;
			$mailBody .= 'Па запыце карыстальніка знойдзена ўнікальных слоў: ' . $this->count_unique_words_in_text . self::BR . self::BR;
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_out.html';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$html = '<html><head>';
			$html .= '<title>Інфармацыя пра частотнасць слоў</title>';
			$html .= '<meta charset="utf-8" />';
			$html .= '<link rel="stylesheet" type="text/css" media="all" href="../../../_css/style.css"/>';
			$html .= "<link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,300,600,700&subset=latin,cyrillic' rel='stylesheet' type='text/css' />";
			$html .= '</head>';
			$html .= '<body>';
			$html .= '<b>ІНФАРМАЦЫЯ ПРА ЧАСТОТНАСЦЬ СЛОЎ</b>' . self::BR . self::BR;
			$html .= $this->result_table;
			$html .= '</body></html>';
			fwrite($newFile, $html);
			fclose($newFile);
			$mailBody .= "Выніковы html-файл захаваны тут: $root/_cache/$serviceName/out/$filename";

			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
			mail($recipient, $subject, $mailBody, $header);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mailBody, $header)));
			fclose($newFile);
		}
		
		public function getResultTable()
		{
			return $this->result_table;
		}
		
		public function getResultCountWordsInText()
		{
			return $this->count_words_in_text;
		}
		
		public function getResultCountUniqueWordsInText()
		{
			return $this->count_unique_words_in_text;
		}
		
		public function getResultByAmount()
		{
			return $this->result_text_by_amount;
		}
		
		public function getResultByAlphabet()
		{
			return $this->result_text_by_alphabet;
		}
		
		public function getResultReverseDictionary()
		{
			return $this->result_text_reverse_dictionary;
		}
		
	}
?>