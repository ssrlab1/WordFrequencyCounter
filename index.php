<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'WordFrequencyCounter.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = WordFrequencyCounter::loadLanguages();
	WordFrequencyCounter::loadLocalization($lang);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo WordFrequencyCounter::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="js/sorttable.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<script>
			var inputTextDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', WordFrequencyCounter::showMessage('default input'))); ?>";
			var stopWordsDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', WordFrequencyCounter::showMessage('default stop words'))); ?>";
			var characterGroup1 = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', WordFrequencyCounter::showMessage('default character group 1'))); ?>";
			var characterGroup2 = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', WordFrequencyCounter::showMessage('default character group 2'))); ?>";
			var wordsToCountDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', WordFrequencyCounter::showMessage('default objective words'))); ?>";
			$(document).ready(function () {
				$(document).on('click', 'button#MainButtonId', function() {
					$('#resultBlockId').show('slow');
					$('#resultId').empty();
					$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
					var caseSensitive = $('input#caseSensitiveId').prop('checked') == true ? 1 : 0;
					var contextSensitive = $('input#contextSensitiveId').prop('checked') == true ? 1 : 0;
					$.ajax({
						type: 'POST',
						url: 'https://corpus.by/WordFrequencyCounter/api.php',
						data: {
							'localization': '<?php echo $lang; ?>',
							'text': $('textarea#inputTextId').val(),
							'stop_words' : $('textarea#stopWordsId').val(),
							'words_to_count' : $('textarea#wordsToCountId').val(),
							'symbols_of_words' : $('textarea#symbolsOfWordsId').val(),
							'symbols_in_words' : $('textarea#symbolsInWordsId').val(),
							'contextsMax' : $('input#contextsMaxId').val(),
							'caseSensitive' : caseSensitive,
							'contextSensitive' : contextSensitive
						},
						success: function(msg) {
							var result = jQuery.parseJSON(msg);
							var wordsCnt = "<?php echo WordFrequencyCounter::showMessage('words amount'); ?><b> " + result.wordsCnt + "</b>";
							var uniqueCnt = "<?php echo WordFrequencyCounter::showMessage('unique words amount'); ?><b> " + result.uniqueCnt + "</b>";
							$('#statisticsId').html(wordsCnt + '<br />' + uniqueCnt);
							$('#resultId').html(result.result);
							tableObject = document.getElementById('resultTableId');
							sorttable.makeSortable(tableObject);
						},
						error: function() {
							$('#resultId').html('ERROR');
						}
					});
				});
				
				$(document).on('click', '#copyButtonId', function() {
					copyToClipboard(document.getElementById("resultTableId"));
				});
				
				function copyToClipboard(elem) {
					// create hidden text element, if it doesn't already exist
					var targetId = "_hiddenCopyText_";
					var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
					var origSelectionStart, origSelectionEnd;
					if (isInput) {
						// can just use the original source element for the selection and copy
						target = elem;
						origSelectionStart = elem.selectionStart;
						origSelectionEnd = elem.selectionEnd;
					} else {
						// must use a temporary form element for the selection and copy
						target = document.getElementById(targetId);
						if (!target) {
							var target = document.createElement("textarea");
							target.style.position = "absolute";
							target.style.left = "-9999px";
							target.style.top = "0";
							target.id = targetId;
							document.body.appendChild(target);
						}
						if(elem.tagName === "TABLE") {
							target.textContent = '';
							var table = document.getElementById("resultTableId");
							for (var r = 0, n = table.rows.length; r < n; r++) {
								for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
									target.textContent += table.rows[r].cells[c].firstChild.textContent + "\t";
								}
								target.textContent += "\n";
							}
						}
						else {
							target.textContent = elem.textContent;
						}
					}
					// select the content
					var currentFocus = document.activeElement;
					target.focus();
					target.setSelectionRange(0, target.value.length);
					
					// copy the selection
					var succeed;
					try {
						  succeed = document.execCommand("copy");
					} catch(e) {
						succeed = false;
					}
					// restore original focus
					if (currentFocus && typeof currentFocus.focus === "function") {
						currentFocus.focus();
					}
					
					if (isInput) {
						// restore prior selection
						elem.setSelectionRange(origSelectionStart, origSelectionEnd);
					} else {
						// clear temporary content
						target.textContent = "";
					}
					return succeed;
				}
			});
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/WordFrequencyCounter/?lang=<?php echo $lang; ?>"><?php echo WordFrequencyCounter::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo WordFrequencyCounter::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo WordFrequencyCounter::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = WordFrequencyCounter::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . WordFrequencyCounter::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-8">
						<div class="control-panel">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="btn-group pull-right">
										<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value=inputTextDefault;"><?php echo WordFrequencyCounter::showMessage('refresh'); ?></button>
										<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value='';"><?php echo WordFrequencyCounter::showMessage('clear'); ?></button>
									</div>
									<h3 class="panel-title"><?php echo WordFrequencyCounter::showMessage('input'); ?></h3>
								</div>
								<div class="panel-body">
									<p><textarea class="form-control" rows="10" id = "inputTextId" name="inputText" placeholder="Enter text"><?php echo str_replace('\n', "\n", WordFrequencyCounter::showMessage('default input')); ?></textarea></p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="control-panel">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="btn-group pull-right">
										<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('stopWordsId').value=stopWordsDefault;"><?php echo WordFrequencyCounter::showMessage('refresh'); ?></button>
										<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('stopWordsId').value='';"><?php echo WordFrequencyCounter::showMessage('clear'); ?></button>
									</div>
									<h3 class="panel-title"><?php echo WordFrequencyCounter::showMessage('ignore'); ?></h3>
								</div>
								<div class="panel-body">
									<p><textarea class="form-control" rows="10" id = "stopWordsId" name="stopWords" placeholder="Enter text"><?php echo str_replace('\n', "\n", WordFrequencyCounter::showMessage('default stop words')); ?></textarea></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('symbolsOfWordsId').value=characterGroup1;"><?php echo WordFrequencyCounter::showMessage('refresh'); ?></button>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('symbolsOfWordsId').value='';"><?php echo WordFrequencyCounter::showMessage('clear'); ?></button>
								</div>
								<h3 class="panel-title"><?php echo WordFrequencyCounter::showMessage('character group 1'); ?></h3>
							</div>
							<div class="panel-body">
								<p><textarea class="form-control" rows="5" id = "symbolsOfWordsId" name="symbolsOfWords" placeholder="Enter text"><?php echo str_replace('\n', "\n", WordFrequencyCounter::showMessage('default character group 1')); ?></textarea></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('symbolsInWordsId').value=characterGroup2;"><?php echo WordFrequencyCounter::showMessage('refresh'); ?></button>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('symbolsInWordsId').value='';"><?php echo WordFrequencyCounter::showMessage('clear'); ?></button>
								</div>
								<h3 class="panel-title"><?php echo WordFrequencyCounter::showMessage('character group 2'); ?></h3>
							</div>
							<div class="panel-body">
								<p><textarea class="form-control" rows="1" id = "symbolsInWordsId" name="symbolsInWords" placeholder="Enter text"><?php echo str_replace('\n', "\n", WordFrequencyCounter::showMessage('default character group 2')); ?></textarea></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('wordsToCountId').value='';"><?php echo WordFrequencyCounter::showMessage('refresh'); ?></button>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('wordsToCountId').value='';"><?php echo WordFrequencyCounter::showMessage('clear'); ?></button>
								</div>
								<h3 class="panel-title"><?php echo WordFrequencyCounter::showMessage('objective words'); ?></h3>
							</div>
							<div class="panel-body">
								<p><textarea class="form-control" rows="1" id = "wordsToCountId" name="wordsToCount" placeholder="Enter text"></textarea></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-6">
						<label for="contextsMaxId"><?php echo WordFrequencyCounter::showMessage('contexts maximum'); ?></label>
						<input type="text" id="contextsMaxId" name="contextsMax"  size="4" maxlength="4" value="2" class="input-primary">
					</div>
					<div class="col-md-6">
						<input type="checkbox" id="caseSensitiveId" name="caseSensitive" value="1" checked>
						<label for="caseSensitiveId"><?php echo WordFrequencyCounter::showMessage('case-sensitive'); ?></label>
						<br />
						<input type="checkbox" id="contextSensitiveId" name="contextSensitive" value="1" checked>
						<label for="contextSensitiveId"><?php echo WordFrequencyCounter::showMessage('context-sensitive'); ?></label>
					</div>
				</div>
				<div class="col-md-12">
					<button type="button" id="MainButtonId" name="MainButton" class="button-primary"><?php echo WordFrequencyCounter::showMessage('button'); ?></button>
				</div>
				<div class="col-md-12" id="resultBlockId" style="display: none;">
					<p id="statisticsId"></p>
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="btn-group pull-right">
									<button type="button" id="copyButtonId" class="btn btn-default btn-xs"><?php echo WordFrequencyCounter::showMessage('copy'); ?></button>
								</div>
								<h3 class="panel-title"><?php echo WordFrequencyCounter::showMessage('result'); ?></h3>
							</div>
							<div class="panel-body">
								<p id="resultId"></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo WordFrequencyCounter::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/WordFrequencyCounter" target="_blank"><?php echo WordFrequencyCounter::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/WordFrequencyCounter/-/issues/new" target="_blank"><?php echo WordFrequencyCounter::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo WordFrequencyCounter::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo WordFrequencyCounter::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $lang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					<?php echo WordFrequencyCounter::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '—', date('Y'); ?>
				</p>
			</div>
		</footer>
	</body>
</html>
<?php WordFrequencyCounter::sendErrorList($lang); ?>