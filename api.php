<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');

	include_once 'WordFrequencyCounter.php';
	$localizationLang = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	WordFrequencyCounter::loadLocalization($localizationLang);

	$text = isset($_POST['text']) ? $_POST['text'] : '';
	$stop_words = isset($_POST['stop_words']) ? $_POST['stop_words'] : '';
	$words_to_count = isset($_POST['words_to_count']) ? $_POST['words_to_count'] : '';
	$symbols_of_words = isset($_POST['symbols_of_words']) ? $_POST['symbols_of_words'] : '';
	$symbols_in_words = isset($_POST['symbols_in_words']) ? $_POST['symbols_in_words'] : '';
	$contextsMax = isset($_POST['contextsMax']) ? $_POST['contextsMax'] : 0;
	$caseSensitive = isset($_POST['caseSensitive']) ? $_POST['caseSensitive'] : '';
	$contextSensitive = isset($_POST['contextSensitive']) ? $_POST['contextSensitive'] : '';

	$msg = '';
	if(!empty($text)) {
		$WordFrequencyCounter = new WordFrequencyCounter($text, $stop_words, $words_to_count, $symbols_of_words, $symbols_in_words, $contextsMax, $caseSensitive, $contextSensitive);
		$WordFrequencyCounter->run();
		$WordFrequencyCounter->saveCacheFiles();
		
		$result['text'] = $text;
		$result['result'] = $WordFrequencyCounter->getResultTable();
		$result['wordsCnt'] = $WordFrequencyCounter->getResultCountWordsInText();
		$result['uniqueCnt'] = $WordFrequencyCounter->getResultCountUniqueWordsInText();
		$msg = json_encode($result);	
	}
	echo $msg;
?>